#include <stdint.h>
#include <string.h>
#include <stdio.h>
#define rotate_left(x,n) (((x) >> (32 - (n))) | ((x) << (n)))
#define rotate_right(x,n) (((x) << (32 - (n))) | ((x) >> (n)))
int valor_inc,E_D;
uint8_t round_keys[176];
uint64_t  z=0b0011110000101100111001010001001000000111101001100011010111011011;
void print(void * x){
    printf("Results\n");
    for(int i=0;i<8;i++){
        printf("Byte %d: %02x \n",i,*(((uint8_t*)x)+i));
    }
}
void init(const void * keys){
    uint32_t subkeys[4]={};
    for (int i=0;i<4;i++){
        memcpy(subkeys+i,keys+(4*i),4);
    }
    memcpy(&round_keys,&subkeys[0],4);
    uint32_t tmp;
    for (int i=0;i<27;i++){
        tmp=(rotate_right(subkeys[1],8) + subkeys[0]) ^ i;
        subkeys[0]=rotate_left(subkeys[0],3) ^ tmp;
        for(int j=1;j<3;j++){
            *(subkeys+j)=*(subkeys+j+1);
        }
        subkeys[3] = tmp;
        memcpy(round_keys+(4*(i+1)),&subkeys[0],4);
    }
}
void encrypt(const void *text, void * res){
    uint32_t *y=(uint32_t *)res;
    uint32_t *x=(((uint32_t *)res) + 1);
    *y=*(uint32_t *)text;
    *x=*(((uint32_t *)text) + 1);
    for (int i=0;i<27;i++){
        *x=(rotate_right(*x,8) + *y) ^ (*(((uint32_t*)round_keys)+i));
        *y=rotate_left(*y,3) ^ *x;
    }
}
void decrypt(const void *text, void * res){
    uint32_t *y=(uint32_t *)res;
    uint32_t *x=(((uint32_t *)res) + 1);
    *y=*(uint32_t *)text;
    *x=*(((uint32_t *)text) + 1);
    for (int i=26;i>=0;i--){
        *y=rotate_right(*x ^ *y,3);
        *x=rotate_left((*x ^ (*(((uint32_t*)round_keys)+i)))-*y,8);
    }
}
int main(int argc, char **argv){
    if(argc !=2){
        printf("mispeck se trata de una implementación del algoritmo Speck64/128 \n");
        printf("./mispeck E para cifrar y D para descifrar \n");
        return 0;
    }
    uint8_t res[8];
    uint8_t key[] = {0x00,0x01,0x02,0x03,0x08,0x09,0x0a,0x0b,0x10,0x11,0x12,0x13,0x18,0x19,0x1a,0x1b};
    uint8_t plain[] = {0X2D,0X43,0X75,0X74,0X74,0X65,0X72,0X3B};
    uint8_t cipher[] = {0X8B,0X02,0X4E,0X45,0X48,0XA5,0X6F,0X8C};
    init(key);
    if(!strcmp(argv[1],"E")){
        encrypt(plain,res);
        print(res);    
        return 0;
        }
    else if(!strcmp(argv[1],"D")){
        decrypt(cipher,res);
        print(res);
        return 0;
    }
    else
    {
        printf("./mispeck E para cifrar y D para descifrar \n");
        return 0;
    }
}