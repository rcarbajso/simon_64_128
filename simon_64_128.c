#include <stdint.h>
#include <string.h>
#include <stdio.h>
#define shift_one(x) (((x) << 1) | ((x) >> 31))
#define shift_eight(x) (((x) << 8) | ((x) >> 24))
#define shift_two(x) (((x) << 2) | ((x) >> 30))
#define rshift_three(x) (((x) >> 3) | (((x) & 0x7) << 29))
#define rshift_one(x)   (((x) >> 1) | (((x) & 0x1) << 31))
int valor_inc,E_D;
uint8_t round_keys[176];
uint64_t  z=0b0011110000101100111001010001001000000111101001100011010111011011;
void print(void * x){
    printf("Results\n");
    for(int i=0;i<8;i++){
        printf("Byte %d: %02x \n",i,*(((uint8_t*)x)+i));
    }
}
int inc_dec(int i){
    if(E_D)
        return i+1;
    else
        return i-1;
    
}
int check(int i){
    if (E_D)
        return i<44;
    else
        return i>=0;
    
}
void init(const void * keys){
    uint32_t subkeys[4]={};
    uint32_t c =0xFFFFFFFC;
    for (int i=0;i<4;i++){
        memcpy(subkeys+i,keys+(4*i),4);
    }
    memcpy(&round_keys,&subkeys[0],4);
    uint32_t tmp,tmp2;
    for (int i=0;i<43;i++){
        tmp=rshift_three(*(subkeys+3)) ^ *(subkeys+1);
        tmp2=rshift_one(tmp);
        tmp^= *(subkeys);
        tmp^=tmp2;
        tmp^=c^((z>>i)&1);
        for(int j=0;j<3;j++){
            *(subkeys+j)=*(subkeys+j+1);
        }
        *(subkeys+3)=tmp;
        memcpy(round_keys+(4*(i+1)),&subkeys[0],4);

    }
}
void encrypt(void *text, void * res){
    uint32_t *y=(uint32_t *)res;
    uint32_t *x=(((uint32_t *)res) + 1);
    *y=*(uint32_t *)text;
    *x=*(((uint32_t *)text) + 1);
    for (int i=valor_inc;check(i);){
        uint32_t temp1=*x;
        *x=*y^(shift_one(*x)&shift_eight(*x))^shift_two(*x)^*((uint32_t*)(round_keys)+i);
        *y=temp1;
        i=inc_dec(i);
    }
}
int main(int argc, char **argv){
    if(argc !=2){
        printf("misimon se trata de una implementación del algoritmo Simon64/128 \n");
        printf("./misimon E para cifrar y D para descifrar \n");
        return 0;
    }
    uint8_t res[8];
    uint8_t key[] = {0x00, 0x01, 0x02, 0x03, 0x08, 0x09, 0x0A, 0x0B, 0x10, 0x11, 0x12, 0x13, 0x18, 0x19, 0x1A, 0x1B};
    uint8_t plain[] = {0x75, 0x6e, 0x64, 0x20, 0x6c, 0x69, 0x6b, 0x65};
    uint8_t cipher[] = {0x7a, 0xa0, 0xdf, 0xb9, 0x20, 0xfc, 0xc8, 0x44};
    init(key);
    if(!strcmp(argv[1],"E")){
        valor_inc=0;
        E_D=1;
        encrypt(plain,res);
        print(res);    
        return 0;
        }
    else if(!strcmp(argv[1],"D")){
        valor_inc=43;
        E_D=0;
        uint32_t x,y;
        x=*((uint32_t *)cipher);
        y=*(((uint32_t *)cipher)+1);
        *((uint32_t *)cipher)=y;
        *(((uint32_t *)cipher)+1)=x;
        encrypt(cipher,res);
        x=*((uint32_t *)res);
        y=*(((uint32_t *)res)+1);
        *((uint32_t *)res)=y;
        *(((uint32_t *)res)+1)=x;
        print(res);
        return 0;
    }
    else
    {
        printf("./misimon E para cifrar y D para descifrar \n");
        return 0;
    }
}