#include <stdint.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
// Cipher Operation Macros
#define word_size 32
#define rounds 44
#define shift_one(x) (((x) << 1) | ((x) >> (word_size - 1)))
#define shift_eight(x) (((x) << 8) | ((x) >> (word_size - 8)))
#define shift_two(x) (((x) << 2) | ((x) >> (word_size - 2)))
#define rshift_three(x) (((x) >> 3) | (((x) & 0x7) << (word_size - 3)))
#define rshift_one(x)   (((x) >> 1) | (((x) & 0x1) << (word_size - 1)))
uint64_t z_arrays[5] = {0b0001100111000011010100100010111110110011100001101010010001011111,
                        0b0001011010000110010011111011100010101101000011001001111101110001,
                        0b0011001101101001111110001000010100011001001011000000111011110101,
                        0b0011110000101100111001010001001000000111101001100011010111011011,
                        0b0011110111001001010011000011101000000100011011010110011110001011};
uint8_t round_keys[176]={};
void print(uint8_t * ciphertext){
    printf("Results\n");
    for(int i=0;i<8;i++){
        printf("Byte %d: %02x \n",i,ciphertext[i]);
    }
}
void init(const void * keys){
    uint64_t subkeys[4]={};
    uint64_t c =0xFFFFFFFFFFFFFFFC;
    uint32_t mod=(1<<32)-1;
    for (int i=0;i<4;i++){
        memcpy(subkeys+i,keys+(4*i),4);
    }
    memcpy(&round_keys,&subkeys[0],4);
    uint64_t tmp,tmp2;
    for (int i=0;i<rounds-1;i++){
        tmp=rshift_three(*(subkeys+3)) ^ *(subkeys+1);
        tmp2=rshift_one(tmp);
        tmp^= *(subkeys);
        tmp^=tmp2;
        tmp^=c^((z_arrays[3]>>(i%62))&1);
        for(int j=0;j<3;j++){
            *(subkeys+j)=*(subkeys+j+1);
        }
        *(subkeys+3)=tmp&mod;
        memcpy(round_keys+(4*(i+1)),&subkeys[0],4);

    }
}
void  encrypt(const void *plaintext, void * ciphertext){
    uint32_t *y=(uint32_t *)ciphertext;
    uint32_t *x=(((uint32_t *)ciphertext) + 1);
    *y=*(uint32_t *)plaintext;
    *x=*(((uint32_t *)plaintext) + 1);
    for (int i=0;i<44;i++){
        uint32_t temp1=*x;
        *x=*y^(shift_one(*x)&shift_eight(*x))^shift_two(*x)^*((uint32_t*)(round_keys)+i);
        *y=temp1;
    }
}
void decrypt( uint8_t *plaintext,const uint8_t * ciphertext){
    uint32_t * x=(uint32_t *)plaintext;
    uint32_t * y=(((uint32_t *)plaintext) + 1);
    *y=*(uint32_t *)ciphertext;
    *x=*(((uint32_t *)ciphertext) + 1);
    for (int i=43;i>=0;i--){
        uint32_t temp1=*y;
        *y=*x^(shift_one(*y)&shift_eight(*y))^shift_two(*y)^*((uint32_t*)(round_keys)+i);
        *x=temp1;
    }
}
int main(int argc, char **argv){
    //Variables para los resultados de cifrado y descifrado
    uint8_t ciphertext[16];
    uint8_t plaintext[16];
    uint8_t key[] = {};
    uint8_t plain[] = {};
    int fd,size;
    char * fichero_res;
    if (argc<2){
        printf("misimon se trata de una implementación del algoritmo Simon64/128 de la NSA \n");
        return 0;
    }
    if(argv[1]=="E"){
        if(argc!=4){
            printf("Para cifrar tienes que pasar al programa al menos el plaintext y la clave\n");
            return 1;
        }
        if(argc!=5)
        {
            printf("El resultado se escribirá en res.txt\n");
            fichero_res="res.txt";
        }
        else
            fichero_res=argv[4];
        if((fd=open(argv[2],O_RDONLY))<0){
            fprintf(stderr,"No se puede abrir el archivo %s",argv[2]);
            return 1;
        }
        while((size=read(fd,plain,8))>0){

        }
        if((fd=open(argv[3],O_RDONLY))<0){
            fprintf(stderr,"No se puede abrir el archivo %s",argv[3]);
            return 1;
        }
        while((size=read(fd,plain,8))>0){

        }
        fd=open(fichero_res,O_RDONLY);
        init(key);
        encrypt(plain,&ciphertext);
        print(ciphertext);
        
    }
    decrypt(plaintext,ciphertext);
    print(plaintext);
    return 0;
}